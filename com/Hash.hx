typedef Hash<T> = haxe.ds.StringMap<T>;
typedef IntHash<T> = haxe.ds.IntMap<T>;